# AWS CDK and SAM

This is an example project about how AWS CDK and SAM live together. It uses pnpm as the package manager.

## Features

- [TypeScript](https://www.typescriptlang.org/) support
- [AWS CDK](https://aws.amazon.com/cdk/) for modeling provisioning the cloud resources and building with esbuild
- [AWS SAM](https://aws.amazon.com/blogs/compute/better-together-aws-sam-and-aws-cdk/) for local testing
- [Jest](https://jestjs.io/) for testing and coverage reporting
- [ESLint](https://eslint.org/) with [AirBnB-base-typescript](https://github.com/iamturns/eslint-config-airbnb-typescript) for linting
- [Prettier](https://prettier.io/) for code formatting
- [pnpm](https://pnpm.io/) for package management
- [husky](https://github.com/typicode/husky) with [pretty-quick](https://prettier.io/docs/en/precommit.html) for pre-commit hook
- Support for debugging in VSCode Studio via launch settings

## Local development

After cloning, install the packages with `pnpm install`.

## Build

Syntesizing the AWS stack and building the lambdas are done with

```bash
pnpm build
```

### Invoke locally

```bash
pnpm hello # or
pnpm call
```

### Start local API server

```bash
pnpm start
```

When the code is changed, you need to run `pnpm build` to rebuild the lambdas.

Example calls can be found in _tests.rest_-file.

### Run tests

```bash
pnpm test
```

### Run tests with coverage

```bash
pnpm coverage
```

### Lint

```bash
pnpm lint
```

### Upgrade packages

```bash
pnpm update
```

## AWS side

### Install

```bash
cdk bootstrap
cdk deploy
```

### Delete

```bash
cdk destroy
```
