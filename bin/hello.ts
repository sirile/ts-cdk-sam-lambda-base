#!/usr/bin/env node
import 'source-map-support/register'
import { App } from 'aws-cdk-lib'
import { HelloStack } from '../lib/hello-stack'

const app = new App()
new HelloStack(app, 'testing')
