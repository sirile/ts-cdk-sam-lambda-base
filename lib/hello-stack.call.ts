import { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from 'aws-lambda'
import axios, { AxiosResponse } from 'axios'

interface UuidResponse {
  uuid: string
}

export async function handler(event: APIGatewayProxyEventV2): Promise<APIGatewayProxyResultV2> {
  console.log('event 👉', event)
  const response: AxiosResponse<UuidResponse> = await axios.get(`https://httpbin.org/uuid`)
  console.log('response 👉', response)
  return {
    statusCode: 200,
    body: JSON.stringify({ result: `${response.data.uuid}` }),
  }
}
