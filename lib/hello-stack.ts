import { Stack, StackProps } from 'aws-cdk-lib'
import { LambdaRestApi } from 'aws-cdk-lib/aws-apigateway'
import { Construct } from 'constructs'
import { NodejsFunction } from 'aws-cdk-lib/aws-lambda-nodejs'
import { Architecture, Runtime } from 'aws-cdk-lib/aws-lambda'

export class HelloStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props)

    const helloFunction = new NodejsFunction(this, 'hello', {
      architecture: Architecture.ARM_64,
      runtime: Runtime.NODEJS_20_X,
    })

    const helloApi = new LambdaRestApi(this, 'hello-world-api', {
      handler: helloFunction,
      proxy: false,
    })

    const hello = helloApi.root.addResource('hello')
    hello.addMethod('GET')

    const callFunction = new NodejsFunction(this, 'call', {
      architecture: Architecture.ARM_64,
      runtime: Runtime.NODEJS_20_X,
    })

    const callApi = new LambdaRestApi(this, 'call-api', {
      handler: callFunction,
      proxy: false,
    })

    const call = callApi.root.addResource('call')
    call.addMethod('GET')
  }
}
