import { handler } from '../lib/hello-stack.call'
import { APIGatewayProxyEventV2 } from 'aws-lambda'
import axios from 'axios'

jest.mock('axios')
const mockedAxios = axios as jest.Mocked<typeof axios>
const uuid = 'f511b17d-15f8-4286-b285-a099eab31186'

interface CallResponse {
  body: string
  statusCode: number
}

describe('Test the lambda function', () => {
  test('Execution succesfully returns a message', async () => {
    // Mock to always return the same value
    mockedAxios.get.mockResolvedValue({
      data: {
        uuid: uuid,
      },
    })
    const result: CallResponse = (await handler({} as APIGatewayProxyEventV2)) as CallResponse
    expect(result.body).toEqual(`{"result":"${uuid}"}`)
    expect(result.statusCode).toEqual(200)
  })
})
