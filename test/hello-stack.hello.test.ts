import { handler } from '../lib/hello-stack.hello'
import { APIGatewayProxyEventV2 } from 'aws-lambda'

describe('Test the lambda function', () => {
  test('Execution succesfully returns a message', async () => {
    interface HelloResponse {
      body: string
      statusCode: number
    }
    const result: HelloResponse = (await handler({} as APIGatewayProxyEventV2)) as HelloResponse
    expect(result.body).toEqual('{"message":"Successful lambda invocation!"}')
    expect(result.statusCode).toEqual(200)
  })
})
