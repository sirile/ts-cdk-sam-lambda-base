import { App } from 'aws-cdk-lib'
import { Template } from 'aws-cdk-lib/assertions'
import { HelloStack } from '../lib/hello-stack'

describe('Test the generated CDK stack', () => {
  test('Stack contains hello-lambda', () => {
    const app = new App()
    const stack = new HelloStack(app, 'MyTestStack')
    Template.fromStack(stack).hasResourceProperties('AWS::Lambda::Function', {
      Handler: 'index.handler',
    })
  })
})
